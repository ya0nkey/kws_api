package models

import "gorm.io/gorm"

type PostComment struct {
	gorm.Model
	Post   Post    `json:"comment_post"`
	User   User    `json:"comment_user"`
	Text   string  `json:"comment_text"`
	Rating float32 `json:"comment_rating"`
}
