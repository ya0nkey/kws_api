package api

import (
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/oauth"
	"gitlab.com/ya0nkey/kws_api/internal/controllers"
)

var ApiRegister = func(r *chi.Mux) {
	r.Get("/", controllers.Index)
	s := oauth.NewBearerServer(
		"mySecretKey-10101",
		time.Second*120,
		nil,
		nil)
	r.Post("/token", s.UserCredentials)
	r.Post("/auth", s.ClientCredentials)

	r.NotFound(controllers.NotFound)
	r.MethodNotAllowed(controllers.NotAllowed)
}
