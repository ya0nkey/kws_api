package models

type Setting struct {
	Option      string `gorm:"unique" json:"setting_option"`
	Value       string `json:"setting_value"`
	Description string `json:"setting_description"`
}
