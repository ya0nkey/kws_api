package controllers

import "net/http"

// Контроллер главной страницы
func Index(w http.ResponseWriter, r *http.Request) {}

// Контроллер при ненайденной странице (404)
func NotFound(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusNotFound)
	w.Write([]byte("route does not exist"))
}

// Контроллер при котором запрещен доступ (405)
func NotAllowed(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusMethodNotAllowed)
	w.Write([]byte("method is not valid"))
}
