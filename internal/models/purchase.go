package models

import "gorm.io/gorm"

type Purchase struct {
	gorm.Model
	User    User    `json:"purchase_user"`
	Other   Other   `json:"purchase_other"`
	Plastic Plastic `json:"purchase_plastic"`
	Schema  Schema  `json:"purchase_schema"`
	Toy     Toy     `json:"purchase_toy"`
}
