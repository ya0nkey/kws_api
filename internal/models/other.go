package models

import "gorm.io/gorm"

type Other struct {
	gorm.Model
	Author      User     `json:"other_author"`
	Name        string   `json:"other_name"`
	Description string   `json:"other_description"`
	Price       uint     `json:"other_price"`
	Rating      float32  `json:"other_rating"`
	Photo       []Photo  `json:"other_photo"`
	Category    Category `json:"other_category"`
}
