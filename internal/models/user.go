package models

import "gorm.io/gorm"

type User struct {
	gorm.Model
	Login    string  `gorm:"unique" json:"user_login"`
	Password string  `json:"user_password"`
	Bio      string  `json:"user_bio"`
	Avatar   Photo   `json:"user_avatar"`
	Role     string  `json:"user_role"`
	Address  string  `json:"user_address"`
	Email    string  `gorm:"unique" json:"user_email"`
	Phone    string  `gorm:"unique" json:"user_phone"`
	Rating   float32 `json:"user_rating"`
}
