package models

import "gorm.io/gorm"

type ItemComment struct {
	gorm.Model
	Plastic Plastic `json:"comment_plastic"`
	Schema  Schema  `json:"comment_schema"`
	Toy     Toy     `json:"comment_toy"`
	Other   Other   `json:"comment_other"`
	User    User    `json:"comment_user"`
	Text    string  `json:"comment_text"`
	Rating  float32 `json:"comment_rating"`
}
