package models

import "gorm.io/gorm"

type Toy struct {
	gorm.Model
	Name        string   `json:"toy_name"`
	Size        string   `json:"toy_size"`
	Description string   `json:"toy_description"`
	Schema      Schema   `json:"toy_schema"`
	Photo       []Photo  `json:"toy_photo"`
	Price       uint     `json:"toy_price"`
	Author      User     `json:"toy_author"`
	Rating      float32  `json:"toy_rating"`
	Category    Category `json:"toy_category"`
}
