package models

import "gorm.io/gorm"

type Post struct {
	gorm.Model
	Title  string  `json:"post_title"`
	Text   string  `json:"post_text"`
	Rating float32 `json:"post_rating"`
	Author User    `json:"post_author"`
}
