module gitlab.com/ya0nkey/kws_api

go 1.20

require (
	github.com/go-chi/chi/v5 v5.0.8
	github.com/go-chi/cors v1.2.1
	github.com/go-chi/oauth v0.0.0-20210913085627-d937e221b3ef
	gorm.io/gorm v1.25.1
)

require (
	github.com/gofrs/uuid v4.0.0+incompatible // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
)
