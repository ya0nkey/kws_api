package models

import "gorm.io/gorm"

type Schema struct {
	gorm.Model
	Author      User     `json:"schema_author"`
	Name        string   `json:"schema_name"`
	Description string   `json:"schema_description"`
	Price       uint     `json:"schema_price"`
	Rating      float32  `json:"schema_rating"`
	Photo       []Photo  `json:"schema_photo"`
	Category    Category `json:"schema_category"`
	Path        string   `json:"schema_path"`
}
