package models

import "gorm.io/gorm"

type Photo struct {
	gorm.Model
	Name string `gorm:"unique" json:"photo_name"`
	Path string `gorm:"unique" json:"photo_path"`
}
