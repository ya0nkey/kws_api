package models

import "gorm.io/gorm"

type Category struct {
	gorm.Model
	Title       string `gorm:"unique" json:"category_title"`
	Parent      uint   `json:"category_parent"`
	Description string `json:"category_description"`
	Photo       Photo  `json:"category_photo"`
}
