package models

import (
	"gorm.io/gorm"
)

type Apikey struct {
	gorm.Model
	Key      string `gorm:"unique" json:"api_key"`
	IsClosed bool   `gorm:"default:false" json:"api_is_closed"`
	User     User   `json:"api_user"`
}
