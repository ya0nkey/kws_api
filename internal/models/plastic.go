package models

import "gorm.io/gorm"

type Plastic struct {
	gorm.Model
	Author      User     `json:"plastic_author"`
	Name        string   `json:"plastic_name"`
	Description string   `json:"plastic_description"`
	Price       uint     `json:"plastic_price"`
	Rating      float32  `json:"plastic_rating"`
	Photo       []Photo  `json:"plastic_photo"`
	Category    Category `json:"plastic_category"`
}
