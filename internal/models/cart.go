package models

type Cart struct {
	User    User    `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;" json:"cart_user"`
	Plastic Plastic `json:"cart_plastic"`
	Schema  Schema  `json:"cart_schema"`
	Toy     Toy     `json:"cart_toy"`
	Other   Other   `json:"cart_other"`
}
